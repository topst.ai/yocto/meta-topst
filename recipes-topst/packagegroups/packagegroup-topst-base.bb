SUMMARY = "TOPST Core packages for Linux/GNU runtime images"
DESCRIPTION = "The minimal set of packages required to boot the TOPST System"
PR = "r17"

PACKAGE_ARCH = "${MACHINE_ARCH}"

inherit packagegroup

RDEPENDS_${PN} = " \
	dbus \
	alsa-lib \
	alsa-utils \
	alsa-state \
	tzdata \
	tzdata-posix \
	kernel-modules \
    packagegroup-telechips-base-net\
"
