FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRCREV = "refs/tags/topst-d3-pre-v0.1"

SRC_URI += "file://0001-pcie-update-pcie-controller-driver-to-latest.patch"
SRC_URI += "file://0001-enable-UFS-Host-controller.patch"
SRC_URI += "file://0001-TOPST-D3-initial-bring-up.patch"

SRC_URI += "${@bb.utils.contains('INVITE_PLATFORM', 'PCIe-Slave', '', 'file://0001-enable-pci-host.patch', d)}"

RDEPENDS_kernel-modules_remove = "kernel-modules-vpu"

do_change_defconfig_append() {
    echo "# CONFIG_POWERVR_DC_FBDEV is not set"                     >> ${WORKDIR}/defconfig
    echo "# CONFIG_TCC_VPU_DRV is not set"                          >> ${WORKDIR}/defconfig

    if ${@bb.utils.contains('INVITE_PLATFORM', 'dp2hdmi', 'true', 'false', d)}; then
        # DP to HDMI (1920x1080)
        echo "CONFIG_DRM_TCC_LCD_VIC=16"                            >> ${WORKDIR}/defconfig
    else
        # DP mode
        echo "CONFIG_DRM_TCC_LCD_VIC=0"                             >> ${WORKDIR}/defconfig
    fi

    echo "CONFIG_I2C_TCC_V3=y"                                      >> ${WORKDIR}/defconfig
    echo "CONFIG_REGULATOR_DA9121=y"                                >> ${WORKDIR}/defconfig
    echo "# CONFIG_TOUCHSCREEN_INIT_SERDES is not set"              >> ${WORKDIR}/defconfig
    echo "# CONFIG_PM_TCC805X_DA9131_SW_WORKAROUND is not set"      >> ${WORKDIR}/defconfig
    echo "# CONFIG_REGULATOR_DA9062 is not set"                     >> ${WORKDIR}/defconfig

    # PWM
    echo "CONFIG_PWM=y"                                             >> ${WORKDIR}/defconfig
    echo "CONFIG_PWM_SYSFS=y"                                       >> ${WORKDIR}/defconfig
    echo "CONFIG_PWM_TCC=y"                                         >> ${WORKDIR}/defconfig

    # PCI
    echo "CONFIG_PCI=y"                                             >> ${WORKDIR}/defconfig
    echo "CONFIG_PCI_DOLPHIN=y"                                     >> ${WORKDIR}/defconfig

    if ${@bb.utils.contains('INVITE_PLATFORM', 'PCIe-Slave', 'true', 'false', d)}; then
        echo "CONFIG_PCI_DOLPHIN_EP=y"                              >> ${WORKDIR}/defconfig
        echo "CONFIG_PCI_ENDPOINT=y"                                >> ${WORKDIR}/defconfig
        echo "CONFIG_PCI_ENDPOINT_CONFIGFS=y"                       >> ${WORKDIR}/defconfig
        echo "CONFIG_PCI_EPF_TEST=y"                                >> ${WORKDIR}/defconfig
        echo "CONFIG_PCI_ENDPOINT_TEST=y"                           >> ${WORKDIR}/defconfig
    else
        echo "CONFIG_PCI_DOLPHIN_HOST=y"                            >> ${WORKDIR}/defconfig
        echo "CONFIG_PCI_ENDPOINT_TEST=y"                           >> ${WORKDIR}/defconfig
    fi

    echo "CONFIG_MISC_FILESYSTEMS=y"                                >> ${WORKDIR}/defconfig
    echo "CONFIG_SQUASHFS=y"                                        >> ${WORKDIR}/defconfig
    echo "CONFIG_SQUASHFS_XZ=y"                                     >> ${WORKDIR}/defconfig
    echo "CONFIG_SQUASHFS_LZO=y"                                    >> ${WORKDIR}/defconfig
    echo "CONFIG_SQUASHFS_XATTR=y"                                  >> ${WORKDIR}/defconfig

    echo "CONFIG_SECCOMP=y"                                         >> ${WORKDIR}/defconfig
    echo "CONFIG_SECCOMP_FILTER=y"                                  >> ${WORKDIR}/defconfig
    echo "CONFIG_BPF=y"                                             >> ${WORKDIR}/defconfig
    echo "CONFIG_BPF_SYSCALL=y"                                     >> ${WORKDIR}/defconfig
    echo "CONFIG_BPF_LSM=y"                                         >> ${WORKDIR}/defconfig
    echo "CONFIG_CGROUP_BPF=y"                                      >> ${WORKDIR}/defconfig

    echo "CONFIG_SWAP=y"                                            >> ${WORKDIR}/defconfig

    echo "CONFIG_SND_USB_AUDIO=y"                                   >> ${WORKDIR}/defconfig

    echo "CONFIG_VIDEOBUF2_CORE=y"                                  >> ${WORKDIR}/defconfig
    echo "CONFIG_VIDEOBUF2_V4L2=y"                                  >> ${WORKDIR}/defconfig
    echo "CONFIG_VIDEOBUF2_MEMOPS=y"                                >> ${WORKDIR}/defconfig
    echo "CONFIG_VIDEOBUF2_VMALLOC=y"                               >> ${WORKDIR}/defconfig

    echo "CONFIG_MEDIA_CAMERA_SUPPORT=y"                            >> ${WORKDIR}/defconfig
    echo "CONFIG_VIDEO_V4L2=y"                                      >> ${WORKDIR}/defconfig
    echo "CONFIG_VIDEO_V4L2_SUBDEV_API=y"                           >> ${WORKDIR}/defconfig

    if ${@bb.utils.contains('INVITE_PLATFORM', 'USB-WEBCAM', 'true', 'false', d)}; then
        # USB Peripheral Controller
        echo "CONFIG_USB_F_UVC=y"                                   >> ${WORKDIR}/defconfig
        echo "CONFIG_USB_CONFIGFS_F_UVC=y"                          >> ${WORKDIR}/defconfig
        echo "CONFIG_USB_G_WEBCAM=y"                                >> ${WORKDIR}/defconfig

        # Media drivers
        echo "CONFIG_MEDIA_USB_SUPPORT=y"                           >> ${WORKDIR}/defconfig

        # Webcam devices
        echo "CONFIG_USB_VIDEO_CLASS=y"                             >> ${WORKDIR}/defconfig
        echo "CONFIG_USB_VIDEO_CLASS_INPUT_EVDEV=y"                 >> ${WORKDIR}/defconfig
        echo "CONFIG_USB_GSPCA=m"                                   >> ${WORKDIR}/defconfig

        # Memory Management options
        echo "CONFIG_FRAME_VECTOR=y"                                >> ${WORKDIR}/defconfig
    fi

    if ${@bb.utils.contains('INVITE_PLATFORM', 'USB-WiFi-MT7601U', 'true', 'false', d)}; then
        # Networking support > Wireless
        echo "CONFIG_CFG80211=y"                                    >> ${WORKDIR}/defconfig
        echo "CONFIG_NL80211_TESTMODE=y"                            >> ${WORKDIR}/defconfig
        echo "CONFIG_MAC80211=y"                                    >> ${WORKDIR}/defconfig
        echo "CONFIG_RFKILL=y"                                      >> ${WORKDIR}/defconfig

        # USB Device Class drivers
        echo "CONFIG_USB_WDM=y"                                     >> ${WORKDIR}/defconfig

        # Wireless LAN > MediaTek devices
        echo "CONFIG_MT7601U=y"                                     >> ${WORKDIR}/defconfig
        echo "CONFIG_RTL8187=y"                                     >> ${WORKDIR}/defconfig
    fi

    echo "CONFIG_USB_EHCI_TCC=m"                                    >> ${WORKDIR}/defconfig

    # remove CAMIPC
    sed -i '/CONFIG_CAMIPC=y/d' ${WORKDIR}/defconfig
}
