do_fix_compile_error() {
   	if echo "$LDFLAGS" | grep -q "dynamic-linker"; then
		sysroot_lib_path=$(dirname $(dirname $(echo $LDFLAGS | grep -o '=.*' | cut -c 2-)))

		libcpp_path="${sysroot_lib_path}/lib/libstdc++.so.6"
		if [ -f $libcpp_path ]; then
			rm -f $libcpp_path*
		fi

		libcpp_path="${sysroot_lib_path}/usr/lib/libstdc++.so.6"
		if [ -f $libcpp_path ]; then
			rm -f $libcpp_path*
		fi
	fi
}

addtask fix_compile_error before do_compile after do_configure
