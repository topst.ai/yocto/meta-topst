FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += " \
    file://weston.ini \
"

do_install_append() {
    install -m 0644 ${WORKDIR}/weston.ini ${D}${datadir}/weston
}
