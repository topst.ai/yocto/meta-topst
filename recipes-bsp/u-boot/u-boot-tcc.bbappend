FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRCREV = "refs/tags/topst-d3-pre-v0.1"

SRC_URI += "file://0001-TOPST-D3-initial-bring-up.patch"
SRC_URI += "file://dtc-lexer.patch"

# DP to HDMI (1920x1080)
SRC_URI += "${@bb.utils.contains('INVITE_PLATFORM', 'dp2hdmi', 'file://add-dp2hdmi.cfg', '', d)}"

# DP MST
# if INVITE_PLATFORM is dp-mst and MACHINE is tcc8050-main, add add-dpmst.cfg
CFG_DP_MST = "${@bb.utils.contains_any('MACHINE', 'tcc8050-main', 'file://add-dpmst.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains('INVITE_PLATFORM', 'dp-mst', '${CFG_DP_MST}', '', d)}"
