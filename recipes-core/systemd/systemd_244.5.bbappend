FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://tcc-ehci.conf"
SRC_URI += "file://tcc-ohci.conf"

do_install_append() {
    install -m 0644 ${WORKDIR}/tcc-ehci.conf		${D}${sysconfdir}/modules-load.d/
    install -m 0644 ${WORKDIR}/tcc-ohci.conf		${D}${sysconfdir}/modules-load.d/

    sed -i '/vpu_hevc_enc_dev/d' ${D}${sysconfdir}/modules-load.d/vpu.conf
}
